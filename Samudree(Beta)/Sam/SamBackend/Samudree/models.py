from django.db import models
from django.utils import timezone

from datetime import datetime, timedelta

class PFZ(models.Model):
	coast_name = models.CharField(max_length=20, null=False)
	coast_lat = models.DecimalField(max_digits=9, decimal_places=6)
	coast_long = models.DecimalField(max_digits=9, decimal_places=6)
	fish_zone_lat = models.DecimalField(max_digits=9, decimal_places=6)
	fish_zone_lng =  models.DecimalField(max_digits=9, decimal_places=6)
	fish_zone_distance = models.DecimalField(max_digits=9, decimal_places=6)

	def __str__(self):
		return self.coast_name

class HWA(models.Model):
	TYPES = (

		('Slight', 'Slight'),
		('Moderate', 'Moderate'),
		('High', 'High'),
	)

	expires = models.DateField(default=None)
	headline = models.CharField(max_length=50, null=True)
	message = models.CharField(max_length=100, null=True)
	severity = models.CharField(max_length=20, default=None, choices=TYPES)
	sent = models.DateField(default=None)

	
class Profile(models.Model):
	FISHERMAN = 'FM'
	SAILOR = 'SL'
	GOVERNMENT_OFFICIAL = 'GO'

	TYPES = (
		(FISHERMAN, 'Fisherman'),
		(SAILOR, 'Sailor'),
		(GOVERNMENT_OFFICIAL, 'Government Official')
	)
	name = models.CharField(max_length=20, null=False)
	mobile_number = models.IntegerField(default=None)
	preferred_language = models.CharField(max_length=20, null=False)
	occupation = models.CharField(max_length=20, choices=TYPES)
	preferred_coast = models.CharField(max_length=20, null=False)
	latitude = models.DecimalField(max_digits=9, decimal_places=6)
	longitude = models.DecimalField(max_digits=9, decimal_places=6)
	inEmergency = models.BooleanField(default=False)

	def __str__(self):
		return self.name