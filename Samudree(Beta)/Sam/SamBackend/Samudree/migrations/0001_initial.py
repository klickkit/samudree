# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-04-02 01:06
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='PFZ',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('coast_name', models.CharField(max_length=20)),
                ('coast_lat', models.DecimalField(decimal_places=6, max_digits=9)),
                ('coast_long', models.DecimalField(decimal_places=6, max_digits=9)),
                ('fish_zone_lat', models.DecimalField(decimal_places=6, max_digits=9)),
                ('fish_zone_lng', models.DecimalField(decimal_places=6, max_digits=9)),
                ('fish_zone_distance', models.DecimalField(decimal_places=6, max_digits=9)),
            ],
        ),
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=20)),
                ('mobile_number', models.IntegerField(default=None)),
                ('preferred_language', models.CharField(max_length=20)),
                ('occupation', models.CharField(choices=[(b'FM', b'Fisherman'), (b'SL', b'Sailor'), (b'GO', b'Government Official')], max_length=20)),
                ('preferred_coast', models.CharField(max_length=20)),
                ('latitude', models.DecimalField(decimal_places=6, max_digits=9)),
                ('longitude', models.DecimalField(decimal_places=6, max_digits=9)),
                ('inEmergency', models.BooleanField(default=False)),
            ],
        ),
    ]
