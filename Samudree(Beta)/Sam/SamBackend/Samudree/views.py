import datetime
import json
import random
import requests
import sys

from dateutil import parser

from django.views.decorators.csrf import csrf_exempt
from django.http.response import HttpResponse

from Samudree.models import PFZ
from Samudree.models import Profile

from xml.dom.minidom import parseString
import xmlrpclib
import xml.etree.ElementTree as ET

from LatLon import lat_lon

from twilio.rest import TwilioRestClient

import nlp

TWILIO_ACCOUNT_SID = 'AC3952bfbbe9610662a185113df8e7207b'
TWILIO_AUTH_TOKEN = '5a7f2812f2749df189a133d611cb4591'
GEOCODING_ENDPOINT = "https://maps.googleapis.com/maps/api/geocode/xml?"
GEOCODING_KEY = 'AIzaSyBzGirGfNyoFo2c4LrA75OmR6SaH3Ky5hY'


def _geocode_coasts(place_to_be_geocoded):
    url = GEOCODING_ENDPOINT + "address={0}&key={1}".format(place_to_be_geocoded, GEOCODING_KEY)
    geocoding_response = requests.get(url)
    return geocoding_response


def _find_key(somejson, key):
    def val(node):
        e = node.nextSibling
        while e and e.nodeType != e.ELEMENT_NODE:
            e = e.nextSibling
        return (e.getElementsByTagName('string')[0].firstChild.nodeValue if e 
                else None)
    foo_dom = parseString(xmlrpclib.dumps((json.loads(somejson),)))
    return [val(node) for node in foo_dom.getElementsByTagName('name') 
            if node.firstChild.nodeValue in key]

@csrf_exempt
def bot(request):
    if request.method == 'POST':
        key_value_pair = {}
        keys = ['date', 'fishing-place', 'intentName', 'speech']
        received_json_data = json.loads(request.body)
        json_data = byteify(received_json_data)
        request_message = json_data.get('request_message')
        nlp_response = nlp.call_api(1, request_message)
        nlp_response = byteify(nlp_response)
        corresponding_value = _find_key(nlp_response, 'speech')
        response = {
            'reply' : corresponding_value[0]
        }
        print response
        return HttpResponse(json.dumps(response), content_type="application/json")
        '''
        if key_value_pair['fishing-place'] and intentName == 'fishing-go-query' and date:
            if  HWA.objects.filter(sent__lte=date, expires__gte=date).exists:
                response = {
                    'reply' : 'You cant enter the sea. There is a high wave alert',
                }
            else:
                response = {
                    'reply' : 'Its safe, I think you can enter into the sea'
                }
            return HttpResponse(json.dumps(response), content_type="application/json")
        else:
            response = {
                'reply' : key_value_pair['speech']
            }
            return HttpResponse(json.dumps(response), content_type="application/json")
        '''


def hwa(request):
    key_value_pair = {}
    model_keys = ['expires', 'headline', 'message', 'severity', 'sent']
    with open('hwa.json') as data_file:    
        data = json.load(data_file)
    for key in model_keys:
        corresponsing_value = byteify(_find_key(json.dumps(data), key))
        if key == 'expires' or key == 'sent':
            corresponding_value = parser.parse(corresponsing_value)
        key_value_pair[key] = corresponsing_value
    hwa = HWA(**key_value_pair)
    hwa.save()


def byteify(input):
    if isinstance(input, dict):
        return {byteify(key): byteify(value)
                for key, value in input.iteritems()}
    elif isinstance(input, list):
        return [byteify(element) for element in input]
    elif isinstance(input, unicode):
        return input.encode('utf-8')
    else:
        return input

def send_otp(request):  
    r = random.randint(1111,9999)
    return "%04d" % r

@csrf_exempt
def handle_arduino(request):
    if request.method == 'POST':
        name = request.POST['name']
        number = request.POST['number']
        print name, number

@csrf_exempt
def pfz(request):
    if request.method == 'POST':
        received_json_data = json.loads(request.body)
        json_data = byteify(received_json_data)
        input_latitude = float(json_data.get('latitude'))
        input_longitude = float(json_data.get('longitude'))
        input_latlon = lat_lon.LatLon(input_latitude, input_longitude)
        tree = ET.parse('Samudree/SouthTamilnadu.xml')
        root = tree.getroot()
        coast_list = []
        coast_lats = []
        for places in root.iter('LC'):
            coast_list.append(places.attrib)
        for iterator in coast_list:
            for key, value in iterator.items():
                geocoding_response = _geocode_coasts(str(value))
                geo_root = ET.fromstring(geocoding_response.content)
                for latlon in geo_root.iter('location'):
                    coast_latitude = float(latlon.find('lat').text)
                    coast_longitude = float(latlon.find('lng').text)
                    coast_lats.append((coast_latitude, coast_longitude))
                    coast_latlon = lat_lon.LatLon(coast_latitude, coast_longitude)
                    for i in root.iter('LC'):
                        if i.attrib['name'] == value:
                            long_degree = i.find('LONG_DEGREE').text
                            long_minute = i.find('LONG_MINUTE').text
                            long_second = i.find('LONG_SECOND').text
                            lat_degree = i.find('LAT_DEGREE').text
                            lat_minute = i.find('LAT_MINUTE').text
                            lat_second = i.find('LAT_SECOND').text
                            break
                        fishzone_latitude = lat_lon.Latitude(degree = lat_degree, minute = lat_minute, second = lat_second)
                        fishzone_longitude = lat_lon.Longitude(degree = long_degree, minute = long_minute, second = long_second)
                        fishzone_latlon = lat_lon.LatLon(fishzone_latitude, fishzone_longitude)
                        distance = float(coast_latlon.distance(fishzone_latlon))
                        pfz_object = PFZ(coast_name=value, fish_zone_lat=float(fishzone_latitude),
                                                 fish_zone_lng=float(fishzone_longitude), fish_zone_distance=distance, coast_lat=coast_latitude,
                                                 coast_long=coast_longitude)
                        pfz_object.save()
        minimum = sys.maxint
        pfzs = PFZ.objects.all()        
        for coast in pfzs:
            coast_latlon = lat_lon.LatLon(coast.coast_lat, coast.coast_long)
            distance = float(input_latlon.distance(coast_latlon))
            if distance < minimum:
                minimum = distance
                coast_to_be_returned = coast
        response = {
            'latitude' : float(coast_to_be_returned.fish_zone_lat),
            'longitude' : float(coast_to_be_returned.fish_zone_lng),
            'distance_to_coast' : distance,
            'coast_name' : str(coast_to_be_returned.coast_name),
        }
        print response
        return HttpResponse(json.dumps(response), content_type="application/json")
        
def _send_sms(message):
    recipient = '+919952886642'
    body = message
    client = TwilioRestClient(TWILIO_ACCOUNT_SID, TWILIO_AUTH_TOKEN)
    message = client.messages.create(to="+91" + str(recipient), from_="+1409-359-5220",
                                     body=body)



@csrf_exempt
def SOS(request):
    if request.method == 'POST':
        received_json_data = json.loads(request.body)
        json_data = byteify(received_json_data)
        json_data = dict(json_data)
        latitude = json_data.get('latitude')
        longitude = json_data.get('longitude')
        mobile_number = json_data.get('mobile_number')
        profile = Profile.objects.get(mobile_number=mobile_number)
        profile.latitude = latitude
        profile.longitude = longitude
        profile.inEmergency = True
        profile.save()
        emergency_message_body = "{0}, a {1} is under attack at latitude : {2} longitude : {3}".format(profile.name, profile.occupation, profile.latitude, profile.longitude)
        _send_sms(emergency_message_body)


@csrf_exempt
def save_profile(request):
    if request.method == 'POST':
        received_json_data = json.loads(request.body)
        json_data = byteify(received_json_data)
        json_data = dict(json_data)
        name = json_data.get('name')
        occupation = json_data.get('occupation')
        mobile_number = json_data.get('mobile_number')
        latitude = json_data.get('latitude')
        longitude = json_data.get('longitude')
        preferred_coast = json_data.get('preferred_coast')
        profile = Profile(name=name, occupation=occupation, latitude=float(latitude),
                          longitude=float(longitude), mobile_number=int(mobile_number),
                          preferred_coast=preferred_coast)
        profile.save()
        otp = send_otp(request) 
        
        client = TwilioRestClient(TWILIO_ACCOUNT_SID, TWILIO_AUTH_TOKEN)
        message = client.messages.create(to="+91" + str(mobile_number), from_="+1409-359-5220",
                             body="Hey {0}, The OTP for using Samudree is {1}".format(name, otp))
        
        response = {
            'OTP' : otp,
        }
        return HttpResponse(json.dumps(response), content_type="application/json")
    else:
        print "Im here at the GET"
        data = request.GET['data']
        print data
        response = {
        	'Sri' : 'Sricharan',
        }
        return HttpResponse(json.dumps(response), content_type="application/json")
        

