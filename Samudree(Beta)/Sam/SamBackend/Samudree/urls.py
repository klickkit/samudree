from django.conf.urls import include
from django.conf.urls import url

from Samudree.views import bot, pfz, save_profile, handle_arduino


urlpatterns = [
	url(r'^submit', save_profile, name='save_profile'),
	url(r'^arduino', handle_arduino, name='arduino'),
	url(r'^pfz', pfz, name='pfz'),
	url(r'^bot', bot, name='bot'),
]